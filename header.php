<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comes & Bebes - Lanchonete</title>
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="content-top">
            <span id="content-left">
                <a href="<?php bloginfo('url') ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png" alt="Logo Comes e Bebes"></a>
                <div class="input-style">
                    <form action="<?php bloginfo('url') ?>/loja" method="get">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/lupa.png" alt="lupa de busca">
                        <input type="text" id ="s" name="s" placeholder="Busca" value="<?php the_search_query() ?>">
                    </form>
                </div>
            </span>
            <span id="content-right">
                <a href="loja"><button>Faça um pedido</button></a>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/carrinho de compras.png" alt="Carrinho de compras">
                <a href="/minha-conta"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/minha conta.png" alt="Botão minha conta"></a>
            </span>

        </div>
    </header>
