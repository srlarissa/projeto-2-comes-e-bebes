<?php
    
    function comesebebes_add_woocommerce_support(){
        add_theme_support('woocommerce');
        add_theme_support('menus');
    }

    add_action('after_setup_theme','comesebebes_add_woocommerce_support');


    function comesebebes_register_styles(){
        wp_enqueue_style('comesebebes-stylesheet', get_template_directory_uri()."/style.css", array(), "1.0", "all");

    }

    add_action('wp_enqueue_scripts', 'comesebebes_register_styles');


    function comesebebes_register_scripts(){
        wp_enqueue_script('comesebebes_scripts', get_template_directory_uri() . "/main.js", array(), "1.0", true);
    }

    add_action('wp_enqueue_scripts', 'comesebebes_register_scripts');

    function format_products($products){
        $products_final = [];
        foreach($products as $product) {
            $image_id  = $product->get_image_id();
            $image_url = wp_get_attachment_image_src( $image_id, 'slide' )[0];
            $products_final[] = [
                'name' => $product->get_name(),
                'price'=>$product->get_price(),
                'link_prod'=>$product->get_permalink(),
                'img'=>$product->get_image(),
                'img_url'=>$image_url,
            ];
        };

        return $products_final;
    };

    function get_link_category_img(){
        $taxonomy     = 'product_cat';
        $orderby      = 'name';
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title        = '';
        $empty        = 0;

        $args = array(
               'taxonomy'     => $taxonomy,
               'orderby'      => $orderby,
               'show_count'   => $show_count,
               'pad_counts'   => $pad_counts,
               'hierarchical' => $hierarchical,
               'title_li'     => $title,
               'hide_empty'   => $empty
        );
        $categorias_lista = [];
        $categories = get_categories($args);


        if ($categories){

           foreach($categories as $category){

            if($category->name !='Sem categoria'){

                        $category_id = $category->term_id;
                        $img_id = get_term_meta($category_id, 'thumbnail_id', true);
                        $categorias_lista[] = [
                            'name' => $category->name,
                            'id'=> $category_id,
                            'link'=> get_term_link($category_id, 'product_cat'),
                            'img'=> wp_get_attachment_image_src($img_id, 'slide')[0]
                        ];
                };

            };
        };
        return $categorias_lista;
    };
?>