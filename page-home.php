
<?php 
    // Template name: Home
    get_header(); ?>
<main>
    <div class="content-top">
        <h1>Comes&Bebes</h1>
        <h2>O restaurante para todas as fomes.</h2>
    </div>

    <div class="content-bottom">
        <h2>CONHEÇA NOSSA LOJA</h2>

        <?php

            $categoria_final = get_link_category_img();
            foreach($categoria_final as $category){
                if($category['name'] !='Sem categoria'){
                    echo '<a href = "'.$category['link'].'" class = ""><img src="'.$category['img'].'" alt =""></a>';
                    echo '<h1> '.$category['name'].'  </h1>';
                }

            }
        ?>

    </div>

    <div class="content-footer">
        <h2>VISITE NOSSA LOJA FÍSICA</h2>
        <div class="footer-left">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/iframe-placeholder.png">
            <span class="address">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/talheres-endereço.png">
                <address>Rua Lorem Ipsum, 123, Brasil</address>
            </span>
            <span>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/telefone.png">

            </span>
        </div>

        <div class="footer-right">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/img-footer.png">
        </div>
    </div>
    
</main>
<?php get_footer(); ?>
